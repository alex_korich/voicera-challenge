class CreateHighlights < ActiveRecord::Migration[5.1]
  def change
    create_table :highlights do |t|
      t.references :meeting, foreign_key: true
      t.datetime :start_time, null: false
      t.datetime :end_time
      t.text :highligh_text, null: false

      t.timestamps
    end
  end
end
