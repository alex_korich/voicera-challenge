class CreateMeetings < ActiveRecord::Migration[5.1]
  def change
    create_table :meetings do |t|
      t.string :title, null: false
      t.string :organizer_email, null: false
      t.datetime :start_time, null: false
      t.datetime :end_time

      t.timestamps
    end
  end
end
