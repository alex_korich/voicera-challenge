class AddAudioFileToMeetings < ActiveRecord::Migration[5.1]
  def change
    add_column :meetings, :audio_file, :string
  end
end
