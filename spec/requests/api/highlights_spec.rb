# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Highlights', type: :request do
  let(:valid_attributes) do
    build(:highlight).attributes
  end

  let(:invalid_attributes) do
    { start_time: nil, highligh_text: nil }
  end

  let(:new_attributes) do
    { highligh_text: FFaker::Company.name }
  end

  let(:invalid_attributes) do
    { highligh_text: nil }
  end

  describe 'GET api/highlights.json' do
    it 'returns 200' do
      create :highlight
      get api_highlights_path(format: :json)
      expect(response).to have_http_status(200)
    end

    it 'returns json' do
      highlight = create :highlight
      get api_highlights_path(format: :json)
      json = JSON.parse(response.body)
      expect(json[0]['id']).to eq(highlight.id)
    end
  end

  describe 'GET api/highlights/:id/.json' do
    it 'returns 200' do
      highlight = create :highlight
      get api_highlight_path(highlight.id, format: :json)
      expect(response).to have_http_status(200)
    end

    it 'returns json' do
      highlight = create :highlight
      get api_highlight_path(highlight, format: :json)
      json = JSON.parse(response.body)
      expect(json['id']).to eq(highlight.id)
    end
  end

  describe 'POST api/highlights.json' do
    it 'returns 201' do
      post '/api/highlights.json', params: { highlight: valid_attributes }
      expect(response).to have_http_status(201)
    end

    it 'creates new highlight' do
      expect do
        post '/api/highlights.json', params: { highlight: valid_attributes }
      end.to change(Highlight, :count).by(1)
    end
  end

  describe 'PUT api/highlights.json' do
    it 'returns 204' do
      highlight = create :highlight
      put "/api/highlights/#{highlight.id}.json", params: { highlight: new_attributes }
      expect(response).to have_http_status(204)
    end

    it 'changes highlight' do
      highlight = create :highlight
      put "/api/highlights/#{highlight.id}.json", params: { highlight: new_attributes }
      highlight.reload
      expect(highlight.highligh_text).to eq(new_attributes[:highligh_text])
    end
  end

  describe 'DELETE api/highlights/:id.json' do
    it 'returns 204' do
      highlight = create :highlight
      delete "/api/highlights/#{highlight.id}.json"
      expect(response).to have_http_status(204)
    end

    it 'deletes highlight' do
      highlight = create :highlight
      expect do
        delete "/api/highlights/#{highlight.id}.json"
      end.to change(Highlight, :count).by(-1)
    end
  end

  context 'nested routes' do
    before :all do
      @meeting = create :meeting
    end

    before :each do
      @highlight = create :highlight, meeting_id: @meeting.id
    end

    describe 'GET api/meetings/:id/highlights.json' do
      it 'returns 200' do
        get api_meeting_highlights_path(meeting_id: @meeting.id, format: :json)
        expect(response).to have_http_status(200)
      end

      it 'returns json' do
        get api_meeting_highlights_path(meeting_id: @meeting.id, format: :json)
        json = JSON.parse(response.body)
        expect(json[0]['id']).to eq(@highlight.id)
      end

      it 'adds new highlight to meeting#highlights' do
        get api_meeting_highlights_path(meeting_id: @meeting.id, format: :json)

        expect(@meeting.highlights).to eq([@highlight])
      end
    end

    describe 'GET api/meetings/:id/highlights/:id/.json' do
      it 'returns 200' do
        get api_meeting_highlight_path(@highlight.id, meeting_id: @meeting.id, format: :json)
        expect(response).to have_http_status(200)
      end

      it 'returns json' do
        get api_meeting_highlight_path(@highlight.id, meeting_id: @meeting.id, format: :json)
        json = JSON.parse(response.body)
        expect(json['id']).to eq(@highlight.id)
      end
    end

    describe 'POST api/meetings/:id//highlights.json' do
      it 'returns 201' do
        post "/api/meetings/#{@meeting.id}/highlights.json", params: { highlight: valid_attributes }
        expect(response).to have_http_status(201)
      end

      it 'creates new highlight' do
        expect do
          post "/api/meetings/#{@meeting.id}/highlights.json", params: { highlight: valid_attributes }
        end.to change(Highlight, :count).by(1)
      end
    end

    describe 'PUT api/meetings/:id/highlights/:id.json' do
      it 'returns 204' do
        put "/api/meetings/#{@meeting.id}/highlights/#{@highlight.id}.json", params: { highlight: new_attributes }
        expect(response).to have_http_status(204)
      end

      it 'changes highlight' do
        put "/api/meetings/#{@meeting.id}/highlights/#{@highlight.id}.json", params: { highlight: new_attributes }
        @highlight.reload
        expect(@highlight.highligh_text).to eq(new_attributes[:highligh_text])
      end
    end

    describe 'DELETE api/meetings/:id/highlights/:id.json' do
      it 'returns 204' do
        delete "/api/meetings/#{@meeting.id}/highlights/#{@highlight.id}.json"
        expect(response).to have_http_status(204)
      end

      it 'deletes highlight' do
        expect do
          delete "/api/meetings/#{@meeting.id}/highlights/#{@highlight.id}.json"
        end.to change(Highlight, :count).by(-1)
      end
    end
  end
end
