# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Meetings', type: :request do
  let(:valid_attributes) do
    build(:meeting).attributes
  end

  let(:invalid_attributes) do
    { start_time: nil, title: nil }
  end

  let(:new_attributes) do
    { title: FFaker::Company.name }
  end

  let(:invalid_attributes) do
    { title: nil }
  end

  describe 'GET api/meetings.json' do
    it 'returns 200' do
      create :meeting
      get api_meetings_path(format: :json)
      expect(response).to have_http_status(200)
    end

    it 'returns json' do
      meeting = create :meeting
      get api_meetings_path(format: :json)
      json = JSON.parse(response.body)
      expect(json[0]['title']).to eq(meeting.title)
    end
  end

  describe 'GET api/highlights/:id/.json' do
    it 'returns 200' do
      meeting = create :meeting
      get api_meeting_path(meeting.id, format: :json)
      expect(response).to have_http_status(200)
    end

    it 'returns json' do
      meeting = create :meeting
      get api_meeting_path(meeting, format: :json)
      json = JSON.parse(response.body)
      expect(json['id']).to eq(meeting.id)
    end
  end

  describe 'POST api/meetings.json' do
    it 'returns 201' do
      post '/api/meetings.json', params: { meeting: valid_attributes }
      expect(response).to have_http_status(201)
    end

    it 'creates new meeting' do
      expect do
        post '/api/meetings.json', params: { meeting: valid_attributes }
      end.to change(Meeting, :count).by(1)
    end
  end

  describe 'PUT api/meetings.json' do
    it 'returns 204' do
      meeting = create :meeting
      put "/api/meetings/#{meeting.id}.json", params: { meeting: new_attributes }
      expect(response).to have_http_status(204)
    end

    it 'changes meeting' do
      meeting = create :meeting
      put "/api/meetings/#{meeting.id}.json", params: { meeting: new_attributes }
      meeting.reload
      expect(meeting.title).to eq(new_attributes[:title])
    end
  end

  describe 'DELETE api/meetings/:id.json' do
    it 'returns 204' do
      meeting = create :meeting
      delete "/api/meetings/#{meeting.id}.json"
      expect(response).to have_http_status(204)
    end

    it 'deletes meeting' do
      meeting = create :meeting
      expect do
        delete "/api/meetings/#{meeting.id}.json"
      end.to change(Meeting, :count).by(-1)
    end
  end
end
