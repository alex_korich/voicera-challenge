# frozen_string_literal: true

RSpec.shared_examples "timeable" do 
  it { should validate_presence_of(:start_time) }

  it 'validates end time is after star time' do
    subject.start_time = DateTime.now
    subject.end_time = 1.hours.ago
    expect(subject).not_to be_valid
  end
end
