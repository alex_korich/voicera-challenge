# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/timeable.rb'

RSpec.describe Meeting, type: :model do
  subject { build(:meeting) }

  describe 'associations' do
    it { should have_many(:highlights) }
  end

  describe 'validations' do
    include_examples "timeable"
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:organizer_email) }
    it { should allow_value('aa@bb.cc').for(:organizer_email) }
    it { should_not allow_value('a.b.c').for(:organizer_email) }
  end
end
