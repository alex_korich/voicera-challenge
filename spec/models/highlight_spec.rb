# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/timeable.rb'

RSpec.describe Highlight, type: :model do
  subject { build(:highlight) }

  describe 'associations' do
    it { should belong_to(:meeting) }
  end

  describe 'validations' do
    include_examples "timeable"
    it { should validate_presence_of(:meeting) }
    it { should validate_presence_of(:highligh_text) }
  end
end
