# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'highlights/show', type: :view do
  before(:each) do
    @highlight = assign(:highlight, create(:highlight))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/#{@highlight.start_time}/)
    expect(rendered).to match(/Highlight Text/)
  end
end
