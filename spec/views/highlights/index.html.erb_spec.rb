# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'highlights/index', type: :view do
  before(:each) do
    assign(:highlights, [create(:highlight), create(:highlight)])
  end

  it 'renders a list of highlights' do
    render
    assert_select 'tr>td', text: 'Highlight Text'.to_s, count: 2
  end
end
