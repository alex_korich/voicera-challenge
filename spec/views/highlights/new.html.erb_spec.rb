# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'highlights/new', type: :view do
  before(:each) do
    assign(:highlight, build(:highlight))
  end

  it 'renders new highlight form' do
    render

    assert_select 'form[action=?][method=?]', highlights_path, 'post' do
      assert_select 'select[name=?]', 'highlight[meeting_id]'

      assert_select 'textarea[name=?]', 'highlight[highligh_text]'
    end
  end
end
