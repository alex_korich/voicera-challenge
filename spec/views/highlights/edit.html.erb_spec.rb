# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'highlights/edit', type: :view do
  before(:each) do
    @highlight = assign(:highlight, create(:highlight))
  end

  it 'renders the edit highlight form' do
    render

    assert_select 'form[action=?][method=?]', highlight_path(@highlight), 'post' do
      assert_select 'select[name=?]', 'highlight[meeting_id]'

      assert_select 'textarea[name=?]', 'highlight[highligh_text]'
    end
  end
end
