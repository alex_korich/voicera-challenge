# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'meetings/edit', type: :view do
  before(:each) do
    @meeting = assign(:meeting, create(:meeting))
  end

  it 'renders the edit meeting form' do
    render

    assert_select 'form[action=?][method=?]', meeting_path(@meeting), 'post' do
      assert_select 'input[name=?]', 'meeting[title]'

      assert_select 'input[name=?]', 'meeting[organizer_email]'

      assert_select 'input[name=?]', 'meeting[audio_file]'
    end
  end
end
