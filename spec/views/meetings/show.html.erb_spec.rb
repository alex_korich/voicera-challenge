# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'meetings/show', type: :view do
  before(:each) do
    @meeting = assign(:meeting, create(:meeting))
  end

  it 'renders attributes in <p>' do
    render
    expect(rendered).to match(/#{@meeting.title}/)
    expect(rendered).to match(/#{@meeting.organizer_email}/)
    expect(rendered).to match(/Audio file:/)
  end
end
