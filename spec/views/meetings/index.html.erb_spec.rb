# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'meetings/index', type: :view do
  before(:each) do
    @meetings = assign(:meetings, [create(:meeting), create(:meeting)])
  end

  it 'renders a list of meetings' do
    render
    assert_select 'tr>td', text: 'Meeting Title'.to_s, count: 2
    assert_select 'tr>td', text: @meetings[0].organizer_email.to_s, count: 1
    assert_select 'tr>td', text: @meetings[1].organizer_email.to_s, count: 1
  end
end
