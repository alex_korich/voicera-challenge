# frozen_string_literal: true

FactoryBot.define do
  factory :meeting do
    title 'Meeting Title'
    organizer_email { FFaker::Internet.email }
    start_time '2018-06-14 21:16:01'
    end_time '2018-06-14 21:18:01'
    audio_file do
      Rack::Test::UploadedFile.new(
        Rails.root.join('spec', 'fixtures', 'audio', 'sample.mp3'),
        'audio/mpeg'
      )
    end
  end
end
