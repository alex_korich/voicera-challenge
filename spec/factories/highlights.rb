# frozen_string_literal: true

FactoryBot.define do
  factory :highlight do
    meeting
    start_time '2018-06-14 21:49:33'
    end_time '2018-06-14 21:50:33'
    highligh_text 'Highlight Text'
  end
end
