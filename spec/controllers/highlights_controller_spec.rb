# frozen_string_literal: true

require 'rails_helper'
require_relative 'shared_examples/requests_examples.rb'

RSpec.describe HighlightsController, type: :controller do
  let(:valid_attributes) do
    build(:highlight).attributes
  end

  let(:invalid_attributes) do
    { start_time: nil, highligh_text: nil }
  end

  include_examples "requests", :highlight


  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        { highligh_text: FFaker::Company.name }
      end

      it 'updates the requested highlight' do
        highlight = create :highlight
        put :update, params: { id: highlight.to_param, highlight: new_attributes }
        highlight.reload
        expect(highlight.highligh_text).to eq(new_attributes[:highligh_text])
      end

      it 'redirects to the highlight' do
        highlight = Highlight.create! valid_attributes
        put :update, params: { id: highlight.to_param, highlight: valid_attributes }
        expect(response).to redirect_to(highlight)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        highlight = create :highlight
        put :update, params: { id: highlight.to_param, highlight: invalid_attributes }
        expect(response).to be_success
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested highlight' do
      highlight = create :highlight
      expect do
        delete :destroy, params: { id: highlight.to_param }
      end.to change(Highlight, :count).by(-1)
    end

    it 'redirects to the highlights list' do
      highlight = Highlight.create! valid_attributes
      delete :destroy, params: { id: highlight.to_param }
      expect(response).to redirect_to(root_path)
    end
  end
end
