# frozen_string_literal: true

RSpec.shared_examples "requests" do |model| 
  describe 'GET #index' do
    it 'returns a success response' do
      create model
      get :index, params: {}
      expect(response).to be_success
    end
  end

  describe 'GET #show' do
    it 'returns a success response' do
      subj = create model
      get :show, params: { id: subj.to_param }
      expect(response).to be_success
    end
  end

  describe 'GET #new' do
    it 'returns a success response' do
      get :new, params: {}
      expect(response).to be_success
    end
  end

  describe 'GET #edit' do
    it 'returns a success response' do
      subj = create model
      get :edit, params: { id: subj.to_param }
      expect(response).to be_success
    end
  end

  describe 'POST #create' do
    context 'with valid params' do
      it 'creates a new instance' do
        expect do
          post :create, params: { model => valid_attributes }
        end.to change(model.to_s.capitalize.constantize, :count).by(1)
      end

      it 'redirects to the created instance' do
        post :create, params: { model => valid_attributes }
        expect(response).to redirect_to(model.to_s.capitalize.constantize.last)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'new' template)" do
        post :create, params: { model => invalid_attributes }
        expect(response).to be_success
      end
    end
  end
end
