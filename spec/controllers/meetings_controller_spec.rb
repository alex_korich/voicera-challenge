# frozen_string_literal: true

require 'rails_helper'

RSpec.describe MeetingsController, type: :controller do
  let(:valid_attributes) do
    attributes_for :meeting
  end

  let(:invalid_attributes) do
    { title: nil, organizer_email: 'corrina.stokesdavis.biz', start_time: nil }
  end

  include_examples "requests", :meeting

  describe 'PUT #update' do
    context 'with valid params' do
      let(:new_attributes) do
        { organizer_email: FFaker::Internet.email }
      end

      it 'updates the requested meeting' do
        meeting = create :meeting
        put :update, params: { id: meeting.to_param, meeting: new_attributes }
        meeting.reload
        expect(meeting.organizer_email).to eq(new_attributes[:organizer_email])
      end

      it 'redirects to the meeting' do
        meeting = create :meeting
        put :update, params: { id: meeting.to_param, meeting: valid_attributes }
        expect(response).to redirect_to(meeting)
      end
    end

    context 'with invalid params' do
      it "returns a success response (i.e. to display the 'edit' template)" do
        meeting = create :meeting
        put :update, params: { id: meeting.to_param, meeting: invalid_attributes }
        expect(response).to be_success
      end
    end
  end

  describe 'DELETE #destroy' do
    it 'destroys the requested meeting' do
      meeting = create :meeting
      expect do
        delete :destroy, params: { id: meeting.to_param }
      end.to change(Meeting, :count).by(-1)
    end

    it 'redirects to the meetings list' do
      meeting = create :meeting
      delete :destroy, params: { id: meeting.to_param }
      expect(response).to redirect_to(meetings_url)
    end
  end
end
