# Voicera test app


* ##### Ruby version: 2.5.0
* ##### Rails version: 5.1.6
* ##### DB: SQlite

## How to run

`bundle`
`rails db:create`
`rails db:migrate`
`rails server`

### Time spent: 5 hours