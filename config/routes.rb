Rails.application.routes.draw do
  resources :highlights
  resources :meetings

  root to: "meetings#index"
  namespace 'api' do
    resources :highlights
    resources :meetings, only: [ :index, :create, :show, :update, :destroy ] do
      resources :highlights, only: [ :index, :create, :show, :update, :destroy ]
    end
  end
end
