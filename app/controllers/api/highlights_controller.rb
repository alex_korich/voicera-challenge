# frozen_string_literal: true

module Api
  class HighlightsController < ApplicationController
    before_action :set_highlight, only: %i[show update destroy]

    def index
      @highlights = params[:meeting_id] ? Meeting.find(params[:meeting_id]).highlights : Highlight.all
      render json: @highlights
    end

    def show
      render json: @highlight
    end

    def create
      @highlight = Highlight.new(highlight_params)

      if @highlight.save
        render json: @highlight, status: :created, location: @highlight
      else
        render @highlight.errors.full_messages.as_json, status: :bad_request
      end
    end

    def update
      if @highlight.update(highlight_params)
        head :no_content
      else
        render @highlight.errors.full_messages.as_json, status: :unprocessable_entity
      end
    end

    def destroy
      @highlight.destroy
      head :no_content
    end

    private

    def set_highlight
      @highlight = Highlight.find(params[:id])
    end

    def highlight_params
      params.require(:highlight).permit(:meeting_id, :start_time, :end_time, :highligh_text)
    end
  end
end
