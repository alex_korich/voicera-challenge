# frozen_string_literal: true

module Api
  class MeetingsController < ApplicationController
    before_action :set_meeting, only: %i[show update destroy]

    def index
      @meetings = Meeting.all
      render json: @meetings
    end

    def show
      render json: @meeting
    end

    def create
      @meeting = Meeting.new(meetings_params)

      if @meeting.save
        render json: @meeting, status: :created, location: @meeting
      else
        render @meeting.errors.full_messages.as_json, status: :bad_request
      end
    end

    def update
      if @meeting.update(meetings_params)
        head :no_content
      else
        render @meeting.errors.full_messages.as_json, status: :unprocessable_entity
      end
    end

    def destroy
      @meeting.destroy
      head :no_content
    end

    private

    def set_meeting
      @meeting = Meeting.find(params[:id])
    end

    def meetings_params
      params.require(:meeting).permit(:title, :organizer_email, :start_time, :end_time, :audio_file)
    end
  end
end
