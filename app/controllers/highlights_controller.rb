# frozen_string_literal: true

class HighlightsController < ApplicationController
  before_action :set_highlight, only: %i[show edit update destroy]

  # GET /highlights
  def index
    @highlights = Highlight.all
  end

  # GET /highlights/1
  def show
  end

  # GET /highlights/new
  def new
    @meeting = Meeting.find(params[:meeting_id]) if params[:meeting_id]
    @selected_meeting_id = selected_meeting_id
    @highlight = Highlight.new
  end

  # GET /highlights/1/edit
  def edit
  end

  # POST /highlights
  def create
    @highlight = Highlight.new(highlight_params)

    if @highlight.save
      redirect_to @highlight, notice: 'Highlight was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /highlights/1
  def update
    if @highlight.update(highlight_params)
      redirect_to @highlight, notice: 'Highlight was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /highlights/1
  def destroy
    @highlight.destroy
    redirect_back fallback_location: root_path, notice: 'Highlight was successfully destroyed.'
  end

  private

  def selected_meeting_id
    @highlight.try(:meeting).try(:id) || @meeting.try(:id)
  end

  def set_highlight
    @highlight = Highlight.find(params[:id])
  end

  def highlight_params
    params.require(:highlight).permit(:meeting_id, :start_time, :end_time, :highligh_text)
  end
end
