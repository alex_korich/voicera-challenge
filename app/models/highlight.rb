# frozen_string_literal: true

class Highlight < ApplicationRecord
  include Timeable
  belongs_to :meeting, inverse_of: :highlights

  validates :meeting, presence: true
  validates :highligh_text, presence: true
end
