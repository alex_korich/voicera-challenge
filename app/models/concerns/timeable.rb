# frozen_string_literal: true

module Timeable
  extend ActiveSupport::Concern
  include ActiveModel::Validations

  included do
    validates :start_time, presence: true
    validate :sane_time

    private

    def sane_time
      errors.add(:start_time, 'Please enter sane time range') if end_time.present? &&
                                                                 start_time.present? &&
                                                                 start_time > end_time
    end
  end
end
