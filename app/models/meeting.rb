# frozen_string_literal: true

class Meeting < ApplicationRecord
  include Timeable
  has_many :highlights, dependent: :destroy
  validates :title, presence: true
  validates :organizer_email, presence: true
  validates :organizer_email, format: { with: URI::MailTo::EMAIL_REGEXP }

  mount_uploader :audio_file, AudioFileUploader
end
