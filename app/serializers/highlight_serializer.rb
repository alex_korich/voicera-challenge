# frozen_string_literal: true

class HighlightSerializer < ActiveModel::Serializer
  attributes :id, :start_time, :end_time, :highligh_text
  belongs_to :meeting
end
