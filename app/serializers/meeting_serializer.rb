# frozen_string_literal: true

class MeetingSerializer < ActiveModel::Serializer
  attributes :id, :title, :organizer_email, :start_time, :end_time, :audio_file
  has_many :highlights
end
